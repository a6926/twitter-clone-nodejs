const express = require('express');
const cors = require('cors');
const config = require('config');
const { Client } = require('pg');
const Tweet = require('./tweet');

(async function run() {
  const app = express();
  app.use(cors());
  app.use(express.json());
  app.use(
    express.urlencoded({
      extended: false,
    })
  );

  const client = new Client(config.db);
  client.connect();

  const tweetService = new Tweet(client);

  await tweetService.createTable();

  app.get('/tweets', async (req, res) => {
    const tweets = await tweetService.listTweets();
    res.json(tweets);
  });

  app.post('/tweets', async (req, res) => {
    const { message } = req.body;
    const created = await tweetService.createTweet(message);
    res.status(201).json(created);
  });
  // app.get('/tweets/:id', (req, res) => {});
  // app.delete('/tweets/:id', (req, res) => {});
  // app.get('/tweets/:id/likes', (req, res) => {});
  // app.post('/tweets/:id/likes', (req, res) => {});
  // app.delete('/tweets/:id/likes', (req, res) => {});

  const server = app.listen(8080, () => {
    const { port } = server.address();
    console.log(`[twitter-clone] 🚀 server started and available on http://localhost:${port}`);
  });
})();
