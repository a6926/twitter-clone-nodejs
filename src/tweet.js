const assert = require('assert');

/**
 * @typedef {import('pg').Client} PGClient
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 */

class Tweet {
  /**
   * @type {PGClient}
   */
  #conn;

  /**
   * Constructor
   * @param {PGClient} pgClient
   */
  constructor(pgClient) {
    this.#conn = pgClient;
  }

  async createTable() {
    const query = `
    CREATE TABLE IF NOT EXISTS tweets (
      id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	    created_at TIMESTAMP NOT NULL,
	    message TEXT NOT NULL
    );`;

    await this.#conn.query(query);
  }

  /**
   *
   * @param {Number} total
   */
  async listTweets(total = 50) {
    const res = await this.#conn.query({
      text: 'SELECT * FROM tweets ORDER BY $1 DESC LIMIT $2',
      values: ['created_at', total],
    });
    return res.rows;
  }

  async createTweet(message) {
    assert.ok(message, '"message" parameter is required');
    const res = await this.#conn.query({
      text: 'INSERT INTO tweets(message, created_at) VALUES($1, $2) RETURNING *',
      values: [message, new Date()],
    });
    return res.rows[0];
  }
}
module.exports = Tweet;

// function get(req, res) {
//   // Implement function
// }

// function create(req, res) {
//   // Implement function
// }

// function del(req, res) {
//   // Implement function
// }

// module.exports = {
//   list,
//   get,
//   create,
//   del,
// };
